import React from 'react';
import { Table } from 'reactstrap';
import MemberRow from '../rows/MemberRow';

const MemberTable = (props) => {
	
	let row;

	if (!props.membersAttr) {
		row = (
			<tr>
				<td colSpan="5">No members found.</td>
			</tr>
		)
	} else {
		let i = 0;
		row = props.membersAttr.map(member => {
			return <MemberRow
				key={member._id}
				member={member}
				index={++i}
				deleteMember={props.deleteMember}
				toggle={props.toggle}
				setLoading={props.setLoading}
			/>
		})
	}

	return (
		<Table responsive hover borderless size="sm" className="p-4 rounded">
			<thead>
				<tr>
					<th>#</th>
					<th>Username</th>
					<th>Team</th>
					<th>Position</th>
					<th>Status</th>
					<th>Actions</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				{row}
			</tbody>
		</Table>
	)
}

export default MemberTable;
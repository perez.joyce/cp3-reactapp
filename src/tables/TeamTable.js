import React from 'react';
import { Table } from 'reactstrap';
import TeamRow from '../rows/TeamRow';

const TeamTable = (props) => {
	console.log("TeamsTable", props.teamsAttr)

	let row;
	

	if(!props.teamsAttr) {
		row = (
			<tr>
				<td colSpan="3">
					<em>No teams found...</em>
				</td>
			</tr>
			)
	} else {
		let i = 0;
		row = (
			props.teamsAttr.map(team => {
				return <TeamRow teamAttr={team} key={team._id} index={++i}/>
			})
		)
	}

	return (
		<Table responsive hover borderless size="sm" className="p-4 rounded">
	      <thead>
	        <tr>
	          <th>#</th>
	          <th>name</th>
	          <th>Actions</th>
	        </tr>
	      </thead>
	      <tbody>
	       { row }
	      </tbody>
	    </Table>
	)
}

export default TeamTable;
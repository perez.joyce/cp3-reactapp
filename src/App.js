import React, { useState, useEffect } from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import "./bootstrap.css"; //bootswatch
import "./style.css";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import Loader from 'react-loader-spinner';
import { Container, Row, Col } from 'reactstrap';

//pages
import AppNavbar from './partials/AppNavbar';
import MembersPage from './pages/MembersPage';
import LandingPage from './pages/LandingPage';
import RegisterPage from './pages/RegisterPage';
import TeamsPage from './pages/TeamsPage';
import LoginPage from './pages/LoginPage';
import NotFoundPage from './pages/NotFoundPage';
import MemberProfilePage from './pages/MemberProfilePage';
import ProfilePage from './pages/ProfilePage';

//component
const App = () => {

	const [appData, setAppData] = useState({
		token: localStorage.token,
		username: localStorage.username
	})

	const { token, username } = appData;

	const [loading, setLoading] = useState(false)


	if (loading) {
		return (
			<Container>
				<Row>
					<Col id="loaderContainer">
						<div id="loader">
							<Loader
								type="Plane"
								color="#00BFFF"
								height={200}
								width={200}
							/>
						</div>
					</Col>
				</Row>
			</Container>
		)
	}

	const getCurrentMember = () => {
		return { username, token }
	}

	// <MembersPage username={ username } token={ token }/>
	const Load = (props, page) => {

		//Landing Page
		if (page === "LandingPage") return <LandingPage {...props} />

		//Login Page 
		if (page === "LoginPage" && token) {
			return <Redirect to="/profile" />
		} else if (page === "LoginPage") {
			return <LoginPage {...props} setLoading={setLoading} />
		}

		//Register Page 
		if (page === "RegisterPage" && token) {
			return <Redirect to="/profile" />
		} else if (page === "RegisterPage") {
			return <RegisterPage {...props} setLoading={setLoading} />
		}


		//refactor null
		if (!token) return <Redirect to="/login" />

		if (page === "LogoutPage") {
			localStorage.clear()
			setAppData({
				token,
				username
			})
			return window.location = "/login"
		}

		switch (page) {
			case "MembersPage": return <MembersPage {...props} tokenAttr={token} setLoading={setLoading} />
			case "MemberProfilePage": return <MemberProfilePage {...props} tokenAttr={token} />
			case "TeamsPage": return <TeamsPage {...props} tokenAttr={token} />
			case "ProfilePage": return <ProfilePage {...props} tokenAttr={token} />
			//TasksPage
			//ProfilePage
			// case "LoginPage" : return <LoginPage {...props} tokenAttr={ token }/>
			default: return <NotFoundPage />
		}
	}

	return (
		<BrowserRouter>
			<AppNavbar tokenAttr={token} usernameAttr={username} getCurrentMemberAttr={getCurrentMember()} />
			<Switch>
				<Route exact path="/members/:id" render={(props) => Load(props, "MemberProfilePage")} />
				<Route path="/members" render={(props) => Load(props, "MembersPage")} />
				<Route path="/teams" render={(props) => Load(props, "TeamsPage")} />
				<Route path="/register" render={(props) => Load(props, "RegisterPage")} />
				<Route path="/login" render={(props) => Load(props, "LoginPage")} />
				<Route path="/profile" render={(props) => Load(props, "ProfilePage")} />
				<Route exact path="/" render={(props) => Load(props, "LandingPage")} />
				<Route path="/logout" render={(props) => Load(props, "LogoutPage")} />
				<Route path="*" render={(props) => Load(props, "NotFoundPage")} />
			</Switch>
		</BrowserRouter>
	)
}

export default App


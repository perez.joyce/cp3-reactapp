import React from 'react';
import { Button } from 'reactstrap';

const TeamRow = ({ teamAttr, index }) => {
	// console.log("TeamRow", props)

	// const team = props.teamsAttr

	return (
	
		<tr>
          <th scope="row">{ index }</th>
          <td>{ teamAttr.name }</td>
          <td>
          	<Button color="info" className="mr-1">
          		<i className="fas fa-eye"></i>
          	</Button>
          	<Button color="warning" className="mr-1">
          		<i className="fas fa-edit"></i>
          	</Button>
          	<Button color="danger" className="mr-1">
          		<i className="fas fa-trash-alt"></i>
          	</Button>
          </td>
        </tr>
    
	)
}

export default TeamRow
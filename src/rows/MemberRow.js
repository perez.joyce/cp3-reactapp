import React from 'react';
import { Link } from 'react-router-dom';
import { Button } from 'reactstrap';
import StripeForm from '../forms/StripeForm';

const MemberRow = ({ member, index, deleteMember, toggle, setLoading }) => {

  const { username, position, _id, teamId, isActive, email } = member;
  let deleteBtn = "";
  let amount = 999 * 100; //for now; multiplied by 100 because default is USD cents

  const deleteOnClickHandler = (_id) => {
    setLoading(true)
    deleteMember(_id)
  }


  if (isActive) {
    deleteBtn = (

      <Button color="danger" className="mr-1" size="sm"
        onClick={() => deleteOnClickHandler(_id)}>
        <i className="fas fa-trash-alt"></i>
      </Button>

    )
  }


  return (
    <tr>
      <th scope="row">{index}</th>
      <td>{username}</td>
      <td>{teamId ? teamId.name : "N/A"}</td>
      <td>{position}</td>
      <td>{isActive ? "Active" : "Deactivated"}</td>
      <td>

        <Link className="btn btn-sm border mr-1" to={`/members/${_id}`}>
          <i className="fas fa-edit"></i>
        </Link>

        <Button color="warning" size="sm" className="mr-1" onClick={() => toggle(_id)}>
          <i className="fas fa-edit"></i>
        </Button>

        {deleteBtn}
      </td>
      <td>
        <StripeForm email={email} amount={amount}/>
      </td>
    </tr>
  )
}

export default MemberRow
import React, { useState, useEffect } from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { Link, Redirect } from 'react-router-dom';
import axios from 'axios';
import Swal from 'sweetalert2';
import { URL } from '../config';

const LoginForm = (props) => {

  // STATE
  // object that holds a component's dynamic data
  // console.log(useState("initial value"))

  //useState
  // const [username, setUsername] = useState("test");
  // console.log(username)

  const [formData, setFormData] = useState({
    email: "",
    password: ""
  })

  const [disabledBtn, setDisabledBtn] = useState(true)

  // const [isRedirected, setIsRedirected] = useState(false)

  const { email, password } = formData;

  const onChangeHandler = e => {
    //update the state
    setFormData({
      ...formData,
      [e.target.name]: e.target.value
    })

    // console.log(formData)
  }

  const onSubmitHandler = async e => {
    e.preventDefault();
    props.setLoading(true)

    const member = {
      email,
      password
    }

    try {
      //create config
      const config = {
        headers: {
          'Content-Type': 'application/json'
        }
      }

      //create the body
      const body = JSON.stringify(member)

      //access route using axios
      const res = await axios.post(`${URL}/members/login`, body, config)

      console.log(res)
      localStorage.setItem("token", res.data.token)
      localStorage.setItem("username", res.data.member.username)

      //sweet alert - success
      window.location = "/profile";

    } catch (e) {
      props.setLoading(false)
      localStorage.removeItem("token")
      //sweet alert - error
      // console.log(e.response.data.message)
      console.log(e)
    }
  }

  //USE EFFECT
  // hook that tells the component what to do after EVERY render
  useEffect(() => {
    if (email !== "" && password !== "") {
      setDisabledBtn(false)
    } else {
      setDisabledBtn(true)
    }
  }, [formData])



  // if(isRedirected){
  //   // return <Redirect to="/profile" />
  //    window.location ="/profile"

  // }


  return (
    <Form onSubmit={e => onSubmitHandler(e)}>
      <FormGroup>
        <Label for="email">Email</Label>
        <Input
          type="email"
          name="email"
          id="email"
          value={email}
          onChange={e => onChangeHandler(e)}
          required
        />
      </FormGroup>
      <FormGroup>
        <Label for="password">Password</Label>
        <Input
          type="password"
          name="password"
          id="password"
          value={password}
          onChange={e => onChangeHandler(e)}
          required
          minLength="5"
        />
      </FormGroup>

      <Button
        color="primary"
        className="btn-block mb-3"
        disabled={disabledBtn}
      >Login</Button>
      <p>
        Don't have an account? <Link to="/register">Register</Link>.
      </p>
    </Form>
  );
}

export default LoginForm;
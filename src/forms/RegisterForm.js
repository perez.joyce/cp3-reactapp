import React, { useState, useEffect } from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { Link, Redirect } from 'react-router-dom';
import axios from 'axios';
import Swal from 'sweetalert2';
import { URL } from '../config';

const RegisterForm = (props) => {

  // STATE
  // object that holds a component's dynamic data
  // console.log(useState("initial value"))

  //useState
  // const [username, setUsername] = useState("test");
  // console.log(username)

  const [formData, setFormData] = useState({
    username: "",
    email: "",
    password: "",
    password2: ""
  })

  const [disabledBtn, setDisabledBtn] = useState(true)

  const [isRedirected, setIsRedirected] = useState(false)

  const { username, email, password, password2 } = formData;

  const onChangeHandler = e => {
    // console.log(e);
    // console.log(e.target);

    //Backticks - multi-line and string interpolation
    // console.log(`e.target.name :  ${e.target.name}`)
    // console.log(`e.target.value: ${e.target.value}`)

    //update the state
    setFormData({
      ...formData,
      [e.target.name]: e.target.value
    })

    // console.log(formData)
  }

  const onSubmitHandler = e => {
    e.preventDefault();
    //check if passwords match
    if (password !== password2) {
      console.log("Passwords don't match!")
      Swal.fire({
        title: "Error!",
        text: "Passwords don't match!",
        icon: "error",
        showConfirmationButton: false,
        timer: 3000
      })
    } else {
      props.setLoading(true)
      register()
    }
  }

  const register = async () => {
    try {
      const newMember = {
        username,
        email,
        password
      }

      //create config
      const config = {
        headers: {
          'Content-Type': 'application/json'
        }
      }

      //create the body
      const body = JSON.stringify(newMember)

      //access route using axios
      await axios.post(`${URL}/members`, body, config)
      props.setLoading(false)
      // setIsRedirected(true)
      window.location = "/login"
    } catch (e) {
      props.setLoading(false)
      console.log(e.response.data.message)
    }
  }

  //USE EFFECT
  // hook that tells the component what to do after EVERY render
  useEffect(() => {
    if (username !== "" && email !== "" && password !== "" && password2 !== "") {
      setDisabledBtn(false)
    } else {
      setDisabledBtn(true)
    }
  }, [formData])



  // if (isRedirected) {
  //   return <Redirect to="/login" />
  // }


  return (
    <Form onSubmit={e => onSubmitHandler(e)}>
      <FormGroup>
        <Label for="username">Username</Label>
        <Input
          type="text"
          name="username"
          id="username"
          value={username}
          onChange={e => onChangeHandler(e)}
          maxLength="30"
          pattern="[a-zA-Z0-9]+"
          required
        />
        <FormText>Use alphanumeric characters only.</FormText>
      </FormGroup>
      <FormGroup>
        <Label for="email">Email</Label>
        <Input
          type="email"
          name="email"
          id="email"
          value={email}
          onChange={e => onChangeHandler(e)}
          required
        />
      </FormGroup>
      <FormGroup>
        <Label for="password">Password</Label>
        <Input
          type="password"
          name="password"
          id="password"
          value={password}
          onChange={e => onChangeHandler(e)}
          required
          minLength="5"
        />
      </FormGroup>
      <FormGroup>
        <Label for="password2">Password</Label>
        <Input
          type="password"
          name="password2"
          id="password2"
          value={password2}
          onChange={e => onChangeHandler(e)}
          required
          minLength="5"
        />
      </FormGroup>

      <Button
        color="primary"
        className="btn-block mb-3"
        disabled={disabledBtn}
      >Submit</Button>
      <p>
        Already have an account? <Link to="/login">Login</Link>.
      </p>
    </Form>
  );
}

export default RegisterForm;
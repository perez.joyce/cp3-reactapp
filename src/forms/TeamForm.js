import React from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

const TeamForm = (props) => {
  return (
    <Form>
      <FormGroup>
        <Label for="name">Name</Label>
        <Input type="text" name="name" id="name" />
      </FormGroup>
      
      <Button color="primary">Submit</Button>
    </Form>
  );
}

export default TeamForm;
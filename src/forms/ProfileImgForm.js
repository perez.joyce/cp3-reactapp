import React, { useState, useEffect } from 'react';
import { Button, Form, FormGroup, Input } from 'reactstrap';

const ProfileImgForm = (props) => {
	//state
	const [profilePic, setProfilePic] = useState(undefined)

	const onChangeHandler = (e) => {
		// console.log(e.target)
		setProfilePic(e.target.files[0])
	}

	const onSubmitHandler = (e) => {
		e.preventDefault()
		const formData = new FormData();
		formData.append("upload", profilePic);
		props.updateImage(formData);
	}



	return (
		<Form className="border p-4 rounded" 
		onSubmit={e => onSubmitHandler(e) }>
			<img className="img-fluid" src={`http://localhost:4000/members/${props._id}/upload`} alt="profile pic"/>
			<FormGroup>
				<Input type="file" name="profilePic" 
				onChange={e => onChangeHandler(e)}
				/>
			</FormGroup>
			<Button className="btn-block border">Save Image</Button>
		</Form>
		)
}

export default ProfileImgForm
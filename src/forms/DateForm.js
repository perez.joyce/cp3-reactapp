import React, { useState, useEffect } from 'react';
import DatePicker from 'react-datepicker'
import "react-datepicker/dist/react-datepicker.css";

import { getDay, setHours, setMinutes } from 'date-fns'
import Swal from 'sweetalert2';

import axios from 'axios';

const DateForm = (props) => {

    const [startDate, setStartDate] = useState( setHours(setMinutes(new Date(), 0), 8) );

    //disable weekends
    const isWeekday = date => {
        const day = getDay(date);
        return day !== 0 && day !== 6;
    };

    // time slot starts at 8 with 1 hr and 30 mins interval
    const [bookingHours, setBookingHours] = useState([
        setHours(setMinutes(new Date(), 0), 8), //8
        setHours(setMinutes(new Date(), 30), 9), //9:30
        setHours(setMinutes(new Date(), 0), 11), //11
        setHours(setMinutes(new Date(), 30), 12), //12:30
        setHours(setMinutes(new Date(), 0), 14), //2
        setHours(setMinutes(new Date(), 30), 15), //3:30
        setHours(setMinutes(new Date(), 0), 17)
    ])

    const onSubmitHandler = e => {
        e.preventDefault()

        const selectedSched = startDate.toDateString(); //Thurs Mar 26 2020
        const d = selectedSched.split(" ") // ["Thu", "Mar", "26", "2020"]
    
        //confirm
        Swal.fire({
            icon: 'question',
            title: 'Confirm Selected Schedule',
            html: `${d[1]} ${d[2]}, ${d[3]} (${d[0]})`,
            focusConfirm: false,
            showCloseButton: true
        }).then(response => {
            // console.log(response)
            if(response.value) {

                const config = props.config.headers["Content-Type"] = "application/json";

                const booking = {
                    startDate,
                    toBeBooked: props.toBeBooked,
                }

                const body = JSON.stringify(booking)

                //access backend
                axios.post(`${props.URL}/bookings/me`, body, config)
                .then(res =>{
                    console.log(res)
                })
                .catch(error => {
                    console.log(error)
                })

            } 
        })
    }


    
      return (
          <form onSubmit={e => onSubmitHandler(e)} >
            <DatePicker
                selected={startDate}
                onChange={date => setStartDate(date)}

                /* exclude weekends */
                filterDate={isWeekday}
                
                /* disable previous dates */
                minDate={new Date()}

                showTimeSelect
                /* Set allowed times to book */
                includeTimes={bookingHours}
                
                dateFormat="MMMM d, yyyy h:mm aa"
                inline
            />
            <button>Submit</button>
          </form>
        
      );
}

export default DateForm


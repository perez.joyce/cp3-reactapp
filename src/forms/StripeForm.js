import React from 'react';
import axios from 'axios';
import StripeCheckout from "react-stripe-checkout";
import { URL, PUBLISHABLE_KEY } from '../config';
import Swal from 'sweetalert2';

const StripeForm = ({ email, amount}) =>{
    /*
    The checkout() function “behind the scenes” sends the card info to Stripe and returns a token object. 

    Our function sends this token and the amount to our backend in the body with an axios request to finish the transaction.
    */
    const checkout = token => {
        
        const body = {
            token: token,
            amount: amount,
        };  
        
        axios
            .post(`${URL}/payments`, body)
            .then(response => {
                console.log(response);
                Swal.fire({
                    title: 'Payment Successful!',
                    text: `You successfullly paid PHP ${amount}.`,
                    icon: 'success',
                    showConfirmButton: false,
                    timer: 1500
                })
            })
            .catch(error => {
            console.log("Payment Error: ", error);
            alert("Payment Error");
            });
    }; 

    return (
        <StripeCheckout
            email={email}
            label="Card Payment" //Component button text
            name="MERN Tracker App" //Modal Header
            description="App tagline here."
            panelLabel="Submit" //Submit button in modal
            amount={amount} //Amount in cents $ needs to be multipled by 100
            token={checkout} 
            stripeKey={PUBLISHABLE_KEY}
            billingAddress={false}
            currency="PHP"
            allowRememberMe={false}
        />
    )

}

export default StripeForm
	  
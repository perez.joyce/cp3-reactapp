import React, { useState, useEffect, Fragment } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import MemberForm from '../forms/MemberForm';
import MemberTable from '../tables/MemberTable';
import MemberModal from '../modals/MemberModal';
import axios from 'axios';
import Paginate from '../partials/Paginate';
import { URL } from '../config'

const MembersPage = (props) => {

	const [membersData, setMembersData] = useState({
		token: props.tokenAttr,
		members: [],
		total: undefined,
		limit: 10
	})

	//destructure
	const { token, members, total, limit } = membersData;
	// console.log("MEMBERS", members, members.length)

	const config = {
		headers: {
			Authorization: `Bearer ${token}`
		}
	}

	// const url = "http://localhost:4000"

	//access members from nodeapp
	const getMembers = async (query = "") => {

		try {
			const res = await axios.get(
				`${URL}/members?limit=${limit}${query}`,
				config
			);

			setMembersData({
				...membersData,
				members: res.data.members,
				total: res.data.total,
			})

			props.setLoading(false)

		} catch (e) {
			//SWAL
			props.setLoading(false)
			// console.log(e.response)

		}
	}

	const deleteMember = async (id) => {
		try {
			const res = await axios.delete(`${URL}/members/${id}`, config)
			getMembers()
		} catch (e) {
			//SWAL
			console.log(e)
		}
	}

	// getMembers();
	// useEffect(() => {
	// 	getMembers()
	// }, [setMembersData])

	//Update Member: Modal
	const [modalData, setModalData] = useState({
		modal: false,
		member: {}
	})

	const { modal, member } = modalData;

	const toggle = async (id) => {
		// console.log(typeof id)
		try {
			if (typeof id === 'string') {
				const res = await axios.get(`${URL}/members/${id}`, config)

				return setModalData({
					modal: !modal,
					member: res.data
				})

			}
			setModalData({
				...modalData,
				modal: !modal
			})
		} catch (e) {
			console.log(e.response)
		}
	}

	//GET ALL TEAMS
	const [teams, setTeams] = useState([])

	const getTeams = async () => {
		try {
			const res = await axios.get(`${URL}/teams`, config)
			// console.log(res.data)
			setTeams(res.data)
		} catch (e) {
			console.log(e)
		}
	}

	// useEffect(() => {
	// 	getTeams()
	// }, [setTeams])

	//UPDATE MEMBER
	const updateMember = async (id, updates) => {
		console.log(id, updates)
		try {
			config.headers["Content-Type"] = "application/json";

			const body = JSON.stringify(updates);

			const res = await axios.patch(`${URL}/members/${id}`, body, config)

			setModalData({
				...modalData,
				modal: !modal
			})
			getMembers()
			//SWAL

		} catch (e) {
			console.log(e)
		}
	}

	useEffect(() => {
		getMembers()
		getTeams()
	}, [])



	return (
		<Fragment>
			<Container className="mb-5">
				<Row className="mb-4">
					<Col>
						<h1>Members Page</h1>
					</Col>
				</Row>
				<Row>
					<Col md="4">
						<MemberForm />
					</Col>
					<Col>
						<div>
							<Button className="btn-sm border mr-1"
								onClick={() => getMembers()}>
								Get All
						</Button>
							<Button className="btn-sm border mr-1"
								onClick={() => getMembers("&isActive=true")}>
								Get Active
						</Button>
							<Button className="btn-sm border mr-1"
								onClick={() => getMembers("&isActive=false")}>
								Get Deactivated
						</Button>
						</div>
						<hr />
						<MemberTable
							membersAttr={members}
							deleteMember={deleteMember}
							toggle={toggle}
							setLoading={props.setLoading}
						/>
						<div>
							{total > 10 ? <Paginate getDocuments={getMembers} total={total} limit={limit} /> : ""}
						</div>

					</Col>
				</Row>
			</Container>
			<MemberModal toggle={toggle}
				modal={modal}
				member={member}
				teams={teams}
				updateMember={updateMember}
				setLoading={props.setLoading}
			/>
		</Fragment>
	);
}

export default MembersPage;
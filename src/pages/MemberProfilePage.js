import React, { useState, useEffect } from 'react';
import { Container, Row, Col } from 'reactstrap';
import MemberProfileForm from '../forms/MemberProfileForm';
import ProfileImgForm from '../forms/ProfileImgForm';
import DateForm from '../forms/DateForm'
import axios from 'axios';
import { URL } from '../config';

const MemberProfilePage = (props) => {
	const [ memberData, setMemberData ] = useState({
		token: props.tokenAttr,
		member: {},
		loggedInMemberId: ""
	})

	const { token, member, loggedInMemberId } = memberData

	const config = {
		headers: {
			Authorization: `Bearer ${token}`
		}
	}


	//GET MEMBER
	const getMember = async () => {
		try {
			// console.log("props match params id", props.match.params.id)
			const res = await axios.get(`${URL}/members/${props.match.params.id}`, config)

			setMemberData({
				...memberData,
				member: res.data
			})


		} catch(e) {
			console.log(e)
		}
	}

	useEffect(()=>{
		getMember()
	}, [])

	//GET ALL TEAMS
	const [teams, setTeams] = useState([])

	const getTeams = async () => {
		try{
			
			const res = await axios.get(`${URL}/teams`, config)

			setTeams(res.data)
		} catch (e) {
			console.log(e)
		}
	}

	useEffect(() => {
		getTeams()
	}, [setTeams])

	//POPULATE DROPDOWN


	//updateImage
	const updateImage = async (body) => {
		try {
			console.log(body)

			const res = await axios.post(`${URL}/members/${props.match.params.id}/upload`, body, config);
			//SWAL

			window.location.reload()
		} catch(e) {
			//SWAL
			console.log(e)
		}
	}

	// //logged in member
    // const getLoggedInMemberId = async() => {
	// 	try {
	// 		const res = await axios.get(`${URL}/members/me`, config);
	// 		setMemberData({
	// 			...memberData,
	// 			loggedInMemberId: res.data._id
	// 		})

	// 	} catch(e){
	// 		console.log(e)
	// 	}
	// }


	// useEffect(()=>{
	// 	getLoggedInMemberId()
	// }, [loggedInMemberId])


	return (
		<Container className="my-5">
			<Row className="mb-3">
				<Col>
					<h1>Member Profile</h1>
				</Col>
			</Row>
			<Row className="mb-5">
				<Col>
					<ProfileImgForm _id={props.match.params.id} updateImage={updateImage}/>
				</Col>
				<Col>
					<MemberProfileForm member={member} teams={teams}/>
				</Col>
			</Row>

			<Row>
				<Col>
					<h1>Book Member</h1>
				</Col>
			</Row>
			<Row>
				<Col>
					<DateForm toBeBooked={props.match.params.id} config={config} URL={URL}/>
				</Col>
			</Row>
		</Container>
	)
}

export default MemberProfilePage
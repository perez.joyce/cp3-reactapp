import React, { useState, useEffect } from 'react';
import { Container, Row, Col } from 'reactstrap';
import ProfileImgForm from '../forms/ProfileImgForm';
import axios from 'axios';
import { URL } from '../config';

const ProfilePage = (props) => {

	//state
	const [profileData, setProfileData] = useState({})

	const config = {
		headers: {
			Authorization: `Bearer ${props.tokenAttr}`
		}
	}

	// const url = "http://localhost:4000/members"

	//getMember
	const getMember = async() => {
		try {
			const res = await axios.get(`${URL}/members/me`, config);
			console.log("PROFILE PAGE RES DATA", res.data);
			const member = res.data;

			setProfileData({
				_id: member._id,
				firstName: member.firstName,
				lastName: member.lastName,
				position: member.position,
				username: member.username
			});

		} catch(e){
			console.log(e)
		}
	}

	useEffect(() => {
		getMember()
	}, [])


	//updateMember

	//updateImage
	const updateImage = async (body) => {
		try {
			console.log(body)

			const res = await axios.post(`${URL}/members/upload`, body, config);
			//SWAL

			window.location.reload()
		} catch(e) {
			//SWAL
			console.log(e)
		}
	}


	return (
		<Container className="my-5">
			<Row className="mb-3">
				<Col>
					<h1>Profile Page</h1>
				</Col>
			</Row>
			<Row className="mb-3">
				<Col md="6">
					<ProfileImgForm _id={profileData._id} updateImage={updateImage}/>
				</Col>
			</Row>

			
		</Container>
	)
}

export default ProfilePage
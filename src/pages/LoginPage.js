import React from 'react';
import { Redirect } from 'react-router-dom';
import { Container, Row, Col } from 'reactstrap';
import LoginForm from '../forms/LoginForm';

const LoginPage = (props) => {
	console.log("LoginPage props", props.tokenAttr)

	return (
		<Container>
			<Row>
				<Col>
					<h1>Login Page</h1>
				</Col>
			</Row>
			<Row>
				<Col>
					<LoginForm setLoading={props.setLoading}/>
				</Col>
			</Row>
		</Container>
		)
}

export default LoginPage
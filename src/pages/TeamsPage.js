import React, { useState, useEffect } from 'react';
import { Container, Row, Col } from 'reactstrap';
import TeamForm from '../forms/TeamForm';
import TeamTable from '../tables/TeamTable';
import axios from 'axios';
import { URL } from '../config';

const TeamsPage = (props) => {
	console.log("TeamsPage props", props.tokenAttr)

	const [ teamsData, setTeamsData] = useState({
		token: props.tokenAttr,
		teams: []
	})

	const { token, teams } = teamsData;
	console.log("Teams Component",  teams)

	const getTeams = async () => {
		try {
			const config = {
				headers : { 
					Authorization: `Bearer ${token}`
				}
			}

			const res = await axios.get(`${URL}/teams`, config)

			console.log("Teams Res", res)
			return setTeamsData({
				teams: res.data
			})

		} catch(e) {
			console.log(e.response)
			//Swal
		}
	}

	// getTeams()
	useEffect(()=> {
		getTeams()
	}, [setTeamsData])

  return (
    <Container className="mb-5">
		<Row className="mb-4">
			<Col>
				<h1>Teams Page</h1>
			</Col>
		</Row>
		<Row>
			<Col md="4" className="border">
				<TeamForm/>
			</Col>
			<Col className="border">
				<TeamTable teamsAttr={teams} />
			</Col>
		</Row>
    </Container>
  );
}

export default TeamsPage;
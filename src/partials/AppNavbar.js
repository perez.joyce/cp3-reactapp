import React, { useState, Fragment } from 'react';
import { Link } from 'react-router-dom';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'reactstrap';

const AppNavbar = (props) => {
  console.log("AppNavbar props tokenAttr", props.tokenAttr)
  console.log("AppNavbar props usernameAttr", props.usernameAttr)
  console.log("AppNavbar props getCurrentMemberAttr", props.getCurrentMemberAttr.username)
  // console.log("token attribute", tokenAttr)

  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  let authLinks = "";

  if (!props.tokenAttr) {
    authLinks = (
      <Fragment>
        <Link className="btn btn-primary" to="/login">Login</Link>
        <Link className="btn btn-info ml-3" to="/register">Register</Link>
      </Fragment>
    )
  } else {
    authLinks = (
      <Fragment>
        <Link className="btn btn-link" to="/profile">Welcome, {props.usernameAttr}</Link>
        <Link className="btn btn-primary ml-3" to="/logout">Logout</Link>
      </Fragment>
    )
  }

  return (
    <div className="mb-5">
      <Navbar color="light" light expand="md">
        <NavbarBrand className="font-weight-bold" href="/">reactstrap</NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <NavItem>
              {/*<NavLink href="/components/">Components</NavLink>*/}
              <Link to="/members" className="nav-link">Members</Link>
            </NavItem>
            <NavItem>
              <Link to="/members" className="nav-link">Teams</Link>
            </NavItem>
            <NavItem>
              <Link to="/members" className="nav-link">Tasks</Link>
            </NavItem>
          </Nav>
          {authLinks}
        </Collapse>
      </Navbar>
    </div>
  );
}

export default AppNavbar;